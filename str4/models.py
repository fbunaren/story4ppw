from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Year(models.Model):
    year = models.CharField(max_length=100)

class Friend(models.Model):
    name = models.CharField(max_length=100)
    hobby = models.CharField(max_length=100)
    fav_fd = models.CharField(max_length=100)
    class_year = models.ForeignKey(Year, on_delete =models.CASCADE)
