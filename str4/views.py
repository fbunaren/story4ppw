from django.shortcuts import render

#For adding data to database
from str4.models import Friend, Year
from datetime import datetime, date

from .forms import FriendForm

import traceback as tb

#Method to get object ID
def object_id(x):
    x = str(x)
    x = x.split("(")[1]
    x = x.split(")")[0]
    return x

def index(request):
    response = {}
    return render(request,'index.html',response)

def contact(request):
    response = {}
    return render(request,'contact-form.html',response)

def contact_success(request):
    response = {}
    return render(request,'contact-form-success.html',response)

def friends(request):
    #For receiving request
    friend_db = Friend.objects.all().values()
    response = dict(request.POST)
    get_response = dict(request.GET)
    form = FriendForm(request.POST)
    response_len = 0
    for i in response:
        response_len += len(response[i][0])
        response[i] = response[i][0]
    
    if request.method == 'GET':
        try:
            for i in Friend.objects.all():
                print(i)
                if object_id(i) == get_response['id'][0]:
                    i.delete()
        except:
            #tb.print_exc()
            pass

    #For Adding Data
    if response_len and request.method == 'POST' and form.is_valid():
        #Save Year
        y =  Year.objects.get_or_create(year=response['class_year'])[0]
        y.save()

        #Save Friend
        p = Friend.objects.create(
            name = response['name'],
            hobby = response['hobby'],
            fav_fd = response['fav_fd'],
            class_year = y
            )
        p.save()

    response['friend_list'] = [dict(i) for i in Friend.objects.all().values()]
    response['form'] = FriendForm()

    for i in range(len(response['friend_list'])):
        response['friend_list'][i]['class_year'] = Year.objects.get(pk=response['friend_list'][i]['class_year_id']).year
    return render(request,'friends.html',response)
