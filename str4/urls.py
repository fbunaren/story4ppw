from django.urls import include, path
from django.conf.urls import include
from .views import index
from django.conf import settings
from . import views

urlpatterns = [
    path('', index, name='index'),
    path('index.html', index, name='index'),
    path('contact-form.html', views.contact , name='contact'),\
    path('contact-form-success.html', views.contact_success , name='contact_success'),
    path('friends.html', views.friends , name='friends'),
    ]
