# Generated by Django 2.2.7 on 2020-02-25 16:38

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('str4', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='friend',
            name='birthday',
        ),
    ]
