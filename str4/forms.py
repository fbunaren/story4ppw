from django import forms

class FriendForm(forms.Form):
    name = forms.CharField(label='Name ', max_length=100,widget=forms.TextInput(attrs={'style' : 'width: 100%;background-color: lightgray;border: 1px;font-size: 18;'}))
    hobby = forms.CharField(label='Hobby ', max_length=100,widget=forms.TextInput(attrs={'style' : 'width: 100%;background-color: lightgray;border: 1px;font-size: 18;'}))
    fav_fd = forms.CharField(label='Favorite Food / Drink ', max_length=100,widget=forms.TextInput(attrs={'style' : 'width: 100%;background-color: lightgray;border: 1px;font-size: 18;'}))
    class_year = forms.CharField(label='Class Year ', max_length=100,widget=forms.TextInput(attrs={'style' : 'width: 100%;background-color: lightgray;border: 1px;font-size: 18;'}))